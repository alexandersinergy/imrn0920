console.log("==========NO.1==========")

function range(startNum1, finishNum1)
{
    var nilaiarray1=[];
    if (startNum1<=finishNum1)
    {
        for (var i=startNum1; i<=finishNum1; i++)
        {
            nilaiarray1.push(i);
        }
    }
    else if (startNum1>=finishNum1)
    {
        for (var i=startNum1; i>=finishNum1; i--)
        {
            nilaiarray1.push(i);
        }
    }
    else
    {
        nilaiarray1.push(-1);
    }
    return nilaiarray1
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log("==========NO.2==========")

function rangeWithStep(startNum2, finishNum2, step)
{
    var step = step || 1;
    var nilaiarray2=[];
    if (startNum2<=finishNum2)
    {
        for (var i=startNum2; i<=finishNum2; i+=step)
        {
            nilaiarray2.push(i);
        }
    }
    else if (startNum2>=finishNum2)
    {
        for (var i=startNum2; i>=finishNum2; i-=step)
        {
            nilaiarray2.push(i);
        }
    }
    else
    {
        nilaiarray2.push(1);
    }
    return nilaiarray2
}


console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11,23,3))
console.log(rangeWithStep(5,2,1))
console.log(rangeWithStep(29,2,4))

console.log("==========NO.3==========")

var sum = function(nilaiarray2)
{
    var hasil = 0;
    for (var i =0; i < nilaiarray2.length; i++)
     hasil = hasil + nilaiarray2[i]
     return hasil;
}


console.log(sum(rangeWithStep(1,10)));
console.log(sum(rangeWithStep(5,50,2)));
console.log(sum(rangeWithStep(15,10)));
console.log(sum(rangeWithStep(20,10, 2)));
console.log(sum(rangeWithStep(1)));
console.log(sum(rangeWithStep()));

console.log("==========NO.4==========")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input){
    var data="";
    for (var a=0; a<input.length; a++ ){
      data += 'Nomor ID : ' + input[a][0] + '\n' + 
              'Nama Lengkap : ' + input[a][1] + '\n' + 
              'TTL : ' + input[a].slice(2,4).join(' ') + '\n' + 
              'Hobi : ' + input[a][4]+ '\n' + '\n';
    }
    return data;
  }
  
  console.log(dataHandling(input));

  console.log("==========NO.5==========") 
 
  function balikKata(kata)
{
    var balik = ""
    for (var i = kata.length - 1; i>=0; i--)
    {
        balik += kata[i]
    }
    return balik
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("==========NO.6==========")


var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

dataHandling3(input);

function dataHandling3(data)
{
    var newData = data
    var newNama = data[1] + " Elsharawy"
    var newProvinsi = "Provinsi " + data[2]
    var kelamin = "Pria"
    var sekolah = "SMA International Metro"

    newData.splice(1,1, newNama)
    newData.splice(2,1, newProvinsi)
    newData.splice(4,1,kelamin,sekolah)

    var arrDate = data[3]
    var newDate = arrDate.split("/")
    var bulan1 = newDate[1]
    var bulan = ""

    switch (bulan1)
    {
        case "01":
            bulan = "Januari"
        break;
        case "02": 
            bulan = "Februari"
        break;
        case "03": 
            bulan = "Maret"
        break;
        case "04":
            bulan = "April"
        break;
        case "05":
            bulan = "Mei"
        break;
        case "06":
            bulan = "Juni"
        break;
        case "07":
            bulan = "Juli"
        break;
        case "08":
            bulan = "Agustus"
        break;
        case "09":
            bulan = "September"
        break;
        case "10":
            bulan = "Oktober"
        break;
        case "11":
            bulan = "November"
        break;
        case "12":
            bulan = "Desember"
        break;
        default:
            break;
    }

    var dateJoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1, value2)
        {
            value2-value1;
        }
    )
    var editName = newNama.slice(0,15)
    console.log(newData)
    console.log(bulan)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}
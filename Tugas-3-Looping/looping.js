//No. 1 Looping While
console.log("LOOPING PERTAMA")
var angka1 = 2;
var kalimat1 = "I love coding";
while (angka1 <= 20) {
    console.log(angka1 + " - " + kalimat1);
    angka1 += 2;
}

console.log("LOOPING KEDUA")
var angka2 = 20;
var kalimat2 = "I will become a mobile developer";
while (angka2  >=2) {
    console.log(angka2 + " - " + kalimat2);
    angka2 -= 2;
}

console.log("===================")

// No. 2 Looping menggunakan for
for (var angka3 = 1; angka3<=20; angka3++)

    if ((angka3%3)==0 && (angka3%2)==1) {
        console.log(angka3+ " - I Love Coding ");
    }

    else if ((angka3%2)==0) {
        console.log(angka3+ " - Berkualitas ");
    }

    else if((angka3%2)==1) {
       console.log(angka3 + " - Santai ");
    }  


console.log("===================")
// No. 3 Membuat Persegi Panjang

var baris = 0;
while (baris < 4)
{
    var kolom = 0, output = "";
    while (kolom < 8)
    {
        output += (baris+kolom) % 2 == 2 ? "" : "#";
        kolom++;
    }
    console.log(output);
    baris++;
}

console.log("===================")
// No. 4 Membuat Tangga 

var baris = 7;
   
    for(var i = 1; i<=baris; i++){
      var simbol = "";
    
      for(var j = 1; j<=i; j++){
        simbol += '#';
      }
      console.log(simbol);
    }

console.log("===================")
// No. 5 Membuat Papan Catur

var baris = 0;
while (baris < 8)
{
    var kolom = 0, output = "";
    while (kolom < 8)
    {
        output += (baris+kolom) % 2 == 0 ? " " : "#";
        kolom++;
    }
    console.log(output);
    baris++;
}



var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

async function baca()
{
    let waktu = 10000
    for(let indek = 0; indek < books.length; indek++)
    {
        waktu = await readBooksPromise(waktu, books[indek])
                .then(function(sisa){
                    return sisa;
                })
                .catch(function(sisa){
                    return sisa;
                })
    }
    console.log("habis")
}

baca();
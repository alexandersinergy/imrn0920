
console.log("==============NOMOR 1==============");

const golden = () =>
{
    console.log("this is golden")
}

golden();

console.log("==============NOMOR 2==============");

const newFunction = (firstName, lastName) => {
    return{
      firstName,
      lastName,
      fullName()
      {
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  newFunction("William", "Imoh").fullName()

console.log("==============NOMOR 3==============");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  
const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation,spell);

console.log("==============NOMOR 4==============");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combinedArray = [...west,...east]

console.log(combinedArray);

console.log("==============NOMOR 5==============");

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before);
console.log("==============Nomor 1==============");

function arrayToObject(array)
{
    
    for (var i = 0; i < array.length; ++i){
        var newObj = {}
        var tahunLahir = array[i][3];
        var now = new Date().getFullYear()
        var umurBaru;

        if (tahunLahir && now - tahunLahir > 0){
            umurBaru = now - tahunLahir
        }else{
            umurBaru ="invalid Birth Year"
        }

        
        newObj.firstName = array[i][0],
        newObj.lastName = array[i][1],
        newObj.gender = array[i][2],
        newObj.age = umurBaru
        

        var consoleText = (i + 1) + '. ' + newObj.firstName + ' ' + newObj.lastName + ' : ';

        console.log(consoleText);
        console.log(newObj);
    }    
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

console.log(arrayToObject(people));
console.log(arrayToObject(people2));


console.log("==============Nomor 2==============");

function shoppingTime(memberId, money)
{
    if(!memberId)
    {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if (money < 50000)
    {
        return "Mohon maaf, uang tidak cukup";
    }
    else
    {
        var newObj = {};
        var moneyBuy = money;
        var daftarBeli = [];
        var sepatuStacattu = "Sepatu Stacatu";
        var bajuZoro = "Baju Zoro";
        var bajuHn = "Baju H&N";
        var sweaterUnikLooh = "Sweater Uniklooh";
        var casingHandphone = "Casing Handphone";

        var cek = 0;
        for (var i = 0; moneyBuy >= 50000 && cek ==0; i++)
        {
            if(moneyBuy >= 1500000)
            {
                daftarBeli.push(sepatuStacattu);
                moneyBuy -= 1500000;
            }
            else if(moneyBuy >= 500000)
            {
                daftarBeli.push(bajuZoro);
                moneyBuy -= 500000;
            }
            else if(moneyBuy >= 250000)
            {
                daftarBeli.push(bajuHn);
                moneyBuy -= 250000;
            }
            else if(moneyBuy >= 175000)
            {
                daftarBeli.push(sweaterUnikLooh);
                moneyBuy -= 175000;
            }
            else if(moneyBuy >= 50000)
            {
                for(var j = 0; j <= daftarBeli.length-1; j++)
                {
                    if(daftarBeli[j] == casingHandphone)
                    {
                        cek +=1;
                    }
                }
                if(cek==0)
                {
                    daftarBeli.push(casingHandphone);
                    moneyBuy -= 50000;
                }
                
            }
        }
        newObj.memberId = memberId;
        newObj.money = money;
        newObj.listPurchased = daftarBeli;
        newObj.changeMoney = moneyBuy;
        return newObj;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());


console.log("==============Nomor 3==============");

function naikAngkot(arrayPenumpang)
{
    var rute = ["A", "B", "C", "D", "E", "F"]
    var arrayOutput = []
    if (arrayPenumpang.length <= 0)
    {
        return [];
    }
    for (var i = 0; i < arrayPenumpang.length; i++)
    {
        var objOutput = {}
        var asal = arrayPenumpang[i][1]
        var tujuan = arrayPenumpang[i][2]

        var indexAsal;
        var indexTujuan;
        
        for (var j = 0; j<rute.length; j++){
            if(rute[j] == asal)
            {
                indexAsal = j
            }else if (rute [j]== tujuan)
            {
                indexTujuan= j
            }
        }
    
        var bayar = (indexTujuan - indexAsal) * 2000

        objOutput.penumpang = arrayPenumpang[i][0]
        objOutput.naikDari = asal
        objOutput.tujuan = tujuan
        objOutput.bayar = bayar

        arrayOutput.push(objOutput);
    }
    return arrayOutput;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
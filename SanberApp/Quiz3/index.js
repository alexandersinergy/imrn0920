import React from "react";
import {StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Home from '../Quiz3/screen/Home';
import Login from '../Quiz3/screen/Login';
import Register from '../Quiz3/screen/Register';
import Splash from '../Quiz3/screen/Splash';


const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Splash" component={Splash} />
    <RootStack.Screen name="Register" component={Register} />
    <RootStack.Screen name="Login" component={Login} />
    <RootStack.Screen name="Home" component={Home} />
  </RootStack.Navigator>
);

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    );
  }
}


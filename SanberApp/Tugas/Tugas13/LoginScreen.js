import React from 'react';
import { Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
         TouchableOpacity, KeyboardAvoidingView
       } from 'react-native';

export default function App() {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View style={styles.containerView}>
                    <Image source={require('./asset/logo.png')} />
                    <Text style={styles.loginText}>Login</Text>
                    <View style={styles.formInput}>
                        <Text style={styles.formText}>Username / Email</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.formInput}>
                        <Text style={styles.formText}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>
                    <View style={styles.squareLogin}>
                        <TouchableOpacity style={styles.buttonLogin} >
                            <Text style={styles.textButton}>  Masuk </Text>
                        </TouchableOpacity>
                        <Text style={styles.text1}>atau</Text>
                        <TouchableOpacity style={styles.buttonReg} >
                            <Text style={styles.textButton}> Daftar ? </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerView: {
        backgroundColor: '#fff',
        marginTop: 63,
        alignItems: 'center',
        flex: 1
    },
    loginText: {
        fontSize: 24,
        color: '#003366',
        textAlign: "center",
        marginVertical: 20
    },
    formText: {
        color: '#003366',
    },
    text1: {
        fontSize: 20,
        color: '#3EC6FF',
        textAlign: "center"
    },
    formInput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
        borderColor: '#003366',
        borderWidth: 1
    },
    button1: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    buttonLogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140
    },
    buttonReg: {
        alignItems: "center",
        backgroundColor: "#003366",
        textDecorationColor: '#000',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 10,
        width: 140
    },
    textButton: {
        color: 'white',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    squareLogin: {
        marginTop: 20,
        alignItems: 'center'
    },
});

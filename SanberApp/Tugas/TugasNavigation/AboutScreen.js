import React from 'react';
import { View, Text, Image, ScrollView, TextInput, StyleSheet, Button } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


export default function App() {
    return (
        <ScrollView>
            <View style={styles.container} >
                <Text style={styles.title}>Tentang Saya</Text>
                <FontAwesome5 name={'user-circle'}
                    size={200} solid
                    color="#EFEFEF"
                    style={styles.icon}
                />
                <Text style={styles.name}>Reza Setya N</Text>
                <Text style={styles.job}>React Native Developer</Text>
                <View style={styles.square} >
                    <Text style={styles.title2}>Portofolio</Text>
                    <View style={styles.square2} >
                        <View>
                            <FontAwesome5 name="gitlab" size={40} color="#3EC6FF" style={styles.icon} />
                            <Text style={styles.text2}>@alexandersinergy</Text>
                        </View>
                        <View >
                            <FontAwesome5 name="github" size={40} color="#3EC6FF" style={styles.icon} />
                            <Text style={styles.tex2}>-</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.square} >
                    <Text style={styles.title2}>Hubungi Saya</Text>
                    <View style={styles.square3} >
                        <View style={styles.square4}>
                            <View >
                                <FontAwesome5 name="facebook" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}}>
                                <Text style={styles.text2}>rezanugrahasetya</Text>
                            </View>
                        </View>
                        <View style={styles.square4}>
                            <View >
                                <FontAwesome5 name="instagram" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}} >
                                <Text style={styles.text2}>-</Text>
                            </View>
                        </View>
                        <View style={styles.square4}>
                            <View>
                                <FontAwesome5 name="twitter" size={40} color="#3EC6FF" style={styles.icon} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 10}}>
                                <Text style={styles.text2}>-</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </View>

        </ScrollView>
    );
}


const styles = StyleSheet.create({
    container: {
        marginTop: 64,

    },
    title: {
        fontSize: 36,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    icon: {
        textAlign: "center"
    },
    name: {
        fontSize: 24,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    job: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#3EC6FF",
        textAlign: "center",
        marginBottom : 7
    },
    square: {
        borderRadius: 10,
        borderColor: 'blue',
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#EFEFEF',
        marginBottom : 9
    },
    square2: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    square3: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    square4: {
        height: 50,
        flexDirection: 'row',
        justifyContent: "center",
        marginBottom:2

    },
    title2: {
        fontSize: 18,
        color: "#003366",
    },
    text2: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },


});

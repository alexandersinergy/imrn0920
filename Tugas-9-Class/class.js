class Animal {
   constructor(name,legs,cold_blooded)
   {
       this.animalName = name;
       this.legs = 4;
       this.cold_blooded = false;
   }
   get name()
   {
       return this.animalName;
   }
   yell = () =>
   {
       console.log("Auooo");
   }
   jump = () =>
   {
       console.log("Hop Hop");
   }
}
 
class Frog extends Animal
{
    constructor(name,legs,cold_blooded)
    {
        super(name,legs,cold_blooded)     
    }    
}

class Ape extends Animal
{
    constructor(name,legs,cold_blooded)
    {
        super(name,cold_blooded)
        this.legs = 2
    }
}


class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
}


var sheep = new Animal("shaun");
var sungokong = new Ape("Kera Sakti");
var kodok = new Frog("buduk");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded); 

console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded); 

var clock = new Clock({template: 'h:m:s'});
clock.start();  